/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Xenon
 */
public class GoodsManagement {
    private static ArrayList<Goods> goodsList = new ArrayList<>();
    
    static {
        load();
    }
    
    public static boolean addGoods(Goods item){
        goodsList.add(item);
        save();
        return true;
    }
    // Delete (D)
    public static boolean delGoods(Goods item){
        goodsList.remove(item);
        save();
        return true;
    }
    public static boolean delGoods(int index){
        goodsList.remove(index);
        save();
        return true;
    }
     public static boolean delAll(){
        int len = goodsList.size();
        for (int i = 0; i < len; i++) {
            goodsList.remove(0);
        }
        save();
        return true;
    }
    // Read (R)
    public static ArrayList<Goods> getGoods(){
        return goodsList;
    }
    public static Goods getGoods(int index) {
        return goodsList.get(index);
    }
    //Update (U)
    public static boolean updateGoods(int index, Goods Goods) {
        goodsList.set(index, Goods);
        save();
        return true;
    }
    
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Nun.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(goodsList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoodsManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoodsManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Nun.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            goodsList = (ArrayList<Goods>)ois.readObject();
            fis.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoodsManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoodsManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GoodsManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
